# Colloque « La participation au développement de politiques publiques et projets »

Équipe organisatrice : Elsau Idéale
Lieu : Strasbourg
Date : ../../2018

## Introduction

La participation des citoyennes et citoyens au développement de politiques publiques est rendue effective par des méthodes et outils de sofistication variable. Ces méthodes et outils portent en eux des problématiques et influent sur l'efficacité de la participation ainsi que sur son acceptabilité.

Cas concret : rénovation urbaine du quartier de l'Elsau, avec l'enjeu d'une subvention par l'ANRU dépendant de la tenue d'une consultation citoyenne soulève des questions d'ordre général.

## Thématique

- participation au développement de politiques publiques
- contribution au développement de projets ouverts

## Problématiques actuelles

- les méthodes et outils de participation au développement de politiques publiques sont éparses (différents processus, différents domaines)
- les méthodes et outils classiques et numériques ne sont pas alignés
- l'accaparation du lexique de la participation sans implémentation concrète entache leur réputation
- les processus de participation volontaire induisent des biais

## Format

Conférences

Hackathon — outils numériques et méthodes d'invitation à la participation

Table-ronde

## Programme

En cours de mise en place.

[Rénovation urbaine de l'Elsau - consultation citoyenne et projets citoyens](renovation-urbaine-elsau.md)

## Détails pratiques

En cours de mise en place.
