# Rénovation urbaine de l'Elsau

Consultation citoyenne et projets citoyens.

## Projet de rénovation urbaine et consultation citoyenne

- présentation du projet de la mairie
- exigence de consultation citoyenne posée par l'ANRU
- processus de consultation en 2018

## Projet Elsau idéale

- proposition d'inclusion dans le projet de rénovation urbaine de la mairie
- présentation des éléments techniques du projet
- ensemble de projets sociaux
- engagement citoyen dans le projet

## Média collectif pour la contribution des habitant·e·s

Projet de mise à disposition des habitant·e·s d'un outil d'autonomisation
par rapport aux projets descendants.

Problématiques :
- comment le mettre au service des habitant·e·s de manière effective et avec
une appropriation large ?
- comment le rendre adaptable aux besoins des citoyen·ne·s ?